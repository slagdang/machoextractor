#!/usr/bin/env python3

"""
This work is available under a Creative Commons Attribution 4.0 International License (CC BY 4.0)

For the purposes of this license the title of this work is Macho Extractor.

It is licensed by Steve Bollinger and the original work was published (and may still be available)
at https://bitbucket.org/slagdang/machoextractor .

Information on the CC BY 4.0 license is available at http://creativecommons.org/licenses/by/4.0/
in both human-readable form and fully specified legal terms.
"""

import io
import struct
import uuid
import argparse
import sys
import os
import hexdump
import json
from typing import Tuple

relocationsegmentname = '$$RELOCATION$$'

# This is the type of relocation map emitted by this version of the code.
# 1 is a 32-bit map version 001
relocationmaptype = 1

class MachoSegment(object):
    ''' Owns and manipulates a segment (load command) in a Mach-o file. Can present the
        metadata and extract and the contents of the segment. '''

    def __init__(self, commandTuple: Tuple[bytes, int, int, int, int, int, int, int, int], iostream: io.BufferedIOBase=None):
        ''' Receives the parsed tuple which is a segment_command or
            segment_command_64 (types from documentation) except for the cmd and
            cmdsize fields. If extraction of
            the segment or further dissection is desired the stream should be
            passed too. '''
        self.iostream = iostream
        self._parse_command_tuple(commandTuple)

    def _parse_command_tuple(self, commandTuple: Tuple[bytes, int, int, int, int, int, int, int, int]) -> None:
        if commandTuple is not None:
            self.segname = commandTuple[0].rstrip(b'\0').decode()
            self.vmaddr, self.vmsize, self.fileoff, self.filesize, _, _, self.nsects = commandTuple[1:8]

    def __str__(self): # __repr__ is not overriden to this since this is not unambiguous
        if (self.filesize < self.vmsize):
            seginfostr = f'{self.segname}:{self.vmsize:x}({self.filesize:x})@{self.vmaddr:x}'
        else:
            seginfostr = f'{self.segname}:{self.vmsize:x}@{self.vmaddr:x}'
        return seginfostr

    def stream_extent(self, section: str=None) -> Tuple[int, int, int]:
        ''' Finds the section of the stream (file) occupied by this segment or, if
            specified the section with the passed name. Returns a 3-tuple which
            is the offset in the stream where the stored item data starts, the
            length in the stream of the stored item data, and the number of padding
            bytes which must to be added to make the full item. '''
        if section is None:
            return self.fileoff, self.filesize, self.vmsize - self.filesize
        else:
            return self.sectiondict[section].stream_extent()

    def extract_segment(self, padding: int=None) -> Tuple[bytes,int]:
        ''' Returns a tuple of (segment data, length of zero padding). If padding is specified
            then it will pad out the segment and return a zero amount of padding. '''
        if self.iostream is None:
            raise ValueError("Cannot extract as there is no io object to extract from.")
        streamoffset, streamsize, paddingneeded = self.stream_extent()
        self.iostream.seek(streamoffset,io.SEEK_SET)
        readdata = self.iostream.read(streamsize)
        if (paddingneeded != 0) and (padding is not None):
            readdata += bytes((padding,)) * paddingneeded
            paddingneeded = 0
        return (readdata, paddingneeded)

    def extract_subsegment(self, offset: int, length: int) -> bytes:
        ''' Extracts a portion of the segment, returns it as a bytes type. '''
        if self.iostream is None:
            raise ValueError("Cannot extract as there is no io object to extract from.")
        if (offset < 0 or length < 0):
            raise ValueError(f'Invalid subsegment negative offset {offset} or length {length}')
        streamoffset = self.fileoff + offset
        amtavailinfile = self.vmsize - offset
        if (length > amtavailinfile):
            raise ValueError(f'Subsegment requested length {length} greater than available size {amtavailinfile}')

        if (length > 0):
            self.iostream.seek(streamoffset,io.SEEK_SET)
            return self.iostream.read(length)
        else:
            return b''

    def extract_section(self, sectionname: str, padding: int=None) -> bytes:
        return self.sectiondict[sectionname].extract_section()

class MachoSection(object):
    ''' Owns and manipulates a section description in a segment in a Mach-o file.
        Can present the metadata and extract the sections. '''

    def __init__(self,segment,sectionTuple):
        ''' Receives the parsed tuple defining the section which is a section
            or section_64. Also receives the segment which contains the data. '''
        self.segment = segment
        self._parse_section_tuple(*sectionTuple)

    def _parse_section_tuple(self, sectnamec: bytes, segnamec: bytes, addr: int, size: int,
                             offset: int, align: int, reloff: int, nreloc: int, flags : int,
                             reserved1: int,reserved2: int,reserved3: int=None) -> None:
        ''' Converts a section tuple to broken out data. 64-bit section tuples
            an extra unused item at the end (reserved3). '''
        self.sectname = sectnamec.rstrip(b'\0').decode()
        self.segname = segnamec.rstrip(b'\0').decode()
        self.addr = addr
        self.size = size
        self.offset = offset
        self.align = align
        self.reloff = reloff
        self.nreloc = nreloc
        self.flags = flags
        self.reserved1 = reserved1
        self.reserved2 = reserved2
        _, self.filesize = self.segment_extent()

    def segment_extent(self) -> Tuple[int, int]:
        ''' Returns a 2-tuple which is the offset in the segment where the 
            section starts, and the length of the section data stored in the
            segment. '''
        segmentoffset = self.addr - self.segment.vmaddr
        sectioninfile = max(0, self.segment.filesize - segmentoffset)
        length = min(self.size, sectioninfile)
        return (segmentoffset, length)

    def stream_extent(self) -> Tuple[int, int]:
        ''' Returns a 2-tuple which is the offset in the stream where the 
            section starts, and the length in the stream of the section data '''
        segmentoffset, length = self.segment_extent()
        return segmentoffset + self.segment.fileoff, length

    def extract_section(self) -> bytes:
        ''' Returns the section data that is stored in the stream. '''
        segmentoffset, length = self.segment_extent()
        return self.segment.extract_subsegment(segmentoffset,length)

def round_up_to_alignment(value: int, align: int) -> int:
    ''' Rounds up integer value to a multiple of align. For
        positive numbers. '''
    excess = value % align
    if (excess != 0):
        value += align - excess
    return value

class MachoFile(object):
    ''' Owns and manipulates a stream which is a Mach-o file. Can parse and present the
        contents. Cannot modify contents.

        Some types, etc are from Apple's Mach-o documentation and headers.
    '''

    ''' This is a struct descriptor used to unpack the Mach-o header. It does not include
        the portion for the magic as it must have been already consumed to know which
        descriptor to use. '''
    MarkerMagicDict = {
        0xfeedface: (0, 32, "6I"),
        0xcefaedfe: (1, 32, "6I"),
        0xfeedfacf: (0, 64, "7I"),
        0xcffaedfe: (1, 64, "7I")
    }

    LoadCommandHeaderUnpack = "2I"

    # load command types/indexes
    LC_SEGMENT = 0x01               # 32-bit segment mapping
    LC_SEGMENT64 = 0x19             # 64-bit segment mapping
    LC_UUID = 0x1b                  # UUID

    # unpack commands for segment load commands
    # these omit the first two uint32_ts because they have already been read as part of the header
    LC_SEGMENT_unpack = "16s4I2I2I"
    LC_SEGMENT64_unpack = "16s4Q2I2I"

    LC_SECTION_unpack = "16s16s9I"
    LC_SECTION64_unpack = "16s16s2Q7II"

    def __init__(self, iostream: io.BufferedIOBase=None, filename: str=None):
        ''' Upon creation remembers the init parameters and inspects the parameter passed
            which is a Python iostream. '''
        if filename is not None:
            try:
                self.iostream = open(filename, "rb")
            except IsADirectoryError:
                raise IsADirectoryError("Operations on input directories are not supported")
            except NotADirectoryError:
                raise NotADirectoryError("Input specifies a directory but operations on input directories are not supported")
        else:
            self.iostream = iostream
        self._read_header_and_identify() # determines little_endian, arch_width, cpu_type and more
        self._parse_load_commands()

    def _read_header_and_identify(self) -> None:
        ''' Reads the first part of the stream (python I/O class). Ensures this is Mach-o and sets the following variables:

            little_endian - True if little_endian, false if big
            arch_width - width of arch, 32 or 64 bits.
            cpu_type - 2-tuple of cputype and cpu_subtype
            file_type - type of macho file (filetype in documentation)
            num_commands - number of commands (ncmds in documentation)
            size_of_commands - size of command list (sizeofcmds in documentation)
            file_flags - from file. Can be evalutated as scalar or attributes (flags in documentation)
            load_command_area_offset - offset into the stream of the load commands.

            After reading this the stream position will be at the start of the load commands.
        '''

        self.iostream.seek(0,io.SEEK_SET)
        self.marker_magic = struct.unpack('>I',self.iostream.read(4))[0]
        try:
            self.little_endian, self.arch_width, headerunpack = self.MarkerMagicDict[self.marker_magic]
            self.unpack_endian = '<' if self.little_endian else '>'
            headerunpack = self.unpack_endian + headerunpack
            headertuple = struct.unpack(headerunpack,self.iostream.read(struct.calcsize(headerunpack)))
            self.cpu_type = headertuple[0:2]
            self.file_type, self.num_commands, self.size_of_commands, self.file_flags = headertuple[2:6]
        except:
            raise TypeError('Input io (stream) is not a Mach-o stream.')
        self.load_command_area_offset = self.iostream.tell()

    def _parse_load_commands(self) -> None:
        ''' Reads all the load commands. Yields information about UUIDs and segments (both sizes) found '''

        self.segmentdict = {}
        commandpoffset = self.load_command_area_offset
        for _ in range(self.num_commands):
            self.iostream.seek(commandpoffset,io.SEEK_SET)
            unpacktuple = self.unpack_endian + self.LoadCommandHeaderUnpack
            loadcommand, loadcommandsize = struct.unpack(unpacktuple,self.iostream.read(struct.calcsize(unpacktuple)))
            if (self.LC_UUID == loadcommand): # UUID
                self.uuid = uuid.UUID(bytes=self.iostream.read(16))
            elif (self.LC_SEGMENT == loadcommand): # load command segment 32
                segmentunpack = self.unpack_endian + self.LC_SEGMENT_unpack
                sectionunpack = self.unpack_endian + self.LC_SECTION_unpack
                sectionunpacksize = struct.calcsize(sectionunpack)
                segmentTuple = struct.unpack(segmentunpack,self.iostream.read(struct.calcsize(segmentunpack)))
                segmentobject = MachoSegment(segmentTuple,self.iostream)
                sectiontuples = [MachoSection(segmentobject,struct.unpack(sectionunpack,self.iostream.read(sectionunpacksize))) for t in range(segmentobject.nsects)]
                segmentobject.sectiondict = {x.sectname : x for x in sectiontuples}
                self.segmentdict[segmentobject.segname] = segmentobject
            elif (self.LC_SEGMENT64 == loadcommand): # load command segment 64
                segmentunpack = self.unpack_endian + self.LC_SEGMENT64_unpack
                sectionunpack = self.unpack_endian + self.LC_SECTION64_unpack
                sectionunpacksize = struct.calcsize(sectionunpack)
                segmentTuple = struct.unpack(segmentunpack,self.iostream.read(struct.calcsize(segmentunpack)))
                segmentobject = MachoSegment(segmentTuple,self.iostream)
                sectiontuples = [MachoSection(segmentobject,struct.unpack(sectionunpack,self.iostream.read(sectionunpacksize))) for t in range(segmentobject.nsects)]
                segmentobject.sectiondict = {x.sectname : x for x in sectiontuples}
                self.segmentdict[segmentobject.segname] = segmentobject

            commandpoffset += loadcommandsize # advance to next load command

    def PorcelainPrintMachoSegments(self) -> None:
        sortedsegments = sorted(parsed.segmentdict.items())
        print(sortedsegments)

    def PrettyPrintMachoSegments(self) -> None:
        try:
            sortedsegments = sorted(parsed.segmentdict.items())
            segnameheader = "segment name"
            memaddressheader = "mem address"
            memsizeheader = "mem size"
            fileoffsetheader = "file offset"
            filelenheader = "file len"
            if (self.arch_width <= 32):
                print(f'{segnameheader:16} {memaddressheader:>11} {memsizeheader:>10} {fileoffsetheader:>11} {filelenheader:>10}')
                for segmentname, segmentinfo in sortedsegments:
                    print(f'{segmentname:16}  0x{segmentinfo.vmaddr:08x} 0x{segmentinfo.vmsize:08x}  0x{segmentinfo.fileoff:08x} 0x{segmentinfo.filesize:08x}')
            else:
                print(f'{segnameheader:16} {memaddressheader:>18} {memsizeheader:>18} {fileoffsetheader:>18} {filelenheader:>18}')
                for segmentname, segmentinfo in sortedsegments:
                    print(f'{segmentname:16} 0x{segmentinfo.vmaddr:016x} 0x{segmentinfo.vmsize:016x} 0x{segmentinfo.fileoff:016x} 0x{segmentinfo.filesize:016x}')
        except:
            print('No segments.')

    def PorcelainPrintMachoSections(self) -> None:
        sortedsegments = sorted(parsed.segmentdict.items())
        for segmentname, segmentinfo in sortedsegments:
            print(segmentname, segmentinfo.vmaddr,segmentinfo.vmsize,segmentinfo.fileoff,segmentinfo.filesize)
            for sectionname, section in sorted(segmentinfo.sectiondict.items()):
                print(section.segname,sectionname,section.addr,section.size,section.offset,section.filesize)

    def PrettyPrintMachoSections(self) -> None:
        try:
            sortedsegments = sorted(parsed.segmentdict.items())
            segnameheader = "segment name"
            sectionnameheader = "section name"
            sectionnameseg = "<<SEGMENT>>"
            memaddressheader = "mem address"
            memsizeheader = "mem size"
            fileoffsetheader = "file offset"
            filelenheader = "file len"
            if (self.arch_width <= 32):
                print(f'{segnameheader:16} {sectionnameheader:16} {memaddressheader:>11} {memsizeheader:>10} {fileoffsetheader:>11} {filelenheader:>10}')
                for segmentname, segmentinfo in sortedsegments:
                    print(f'{segmentname:16} {sectionnameseg:16}  0x{segmentinfo.vmaddr:08x} 0x{segmentinfo.vmsize:08x}  0x{segmentinfo.fileoff:08x} 0x{segmentinfo.filesize:08x}')
                    for sectionname, section in sorted(segmentinfo.sectiondict.items()):
                        print(f'{section.segname:16} {sectionname:16}  0x{section.addr:08x} 0x{section.size:08x}  0x{section.offset:08x} 0x{section.filesize:08x}')
            else:
                print(f'{segnameheader:16} {sectionnameheader:16} {memaddressheader:>18} {memsizeheader:>18} {fileoffsetheader:>18} {filelenheader:>18}')
                for segmentname, segmentinfo in sortedsegments:
                    print(f'{segmentname:16} {sectionnameseg:16} 0x{segmentinfo.vmaddr:016x} 0x{segmentinfo.vmsize:016x} 0x{segmentinfo.fileoff:016x} 0x{segmentinfo.filesize:016x}')
                    for sectionname, section in sorted(segmentinfo.sectiondict.items()):
                        print(f'{section.segname:16} {sectionname:16} 0x{section.addr:016x} 0x{section.size:016x} 0x{section.offset:016x} 0x{section.filesize:016x}')

        except:
            print('No segments.')

    ''' returns a tuple of (segment data, length of zero padding). or None if not found. '''
    def ExtractSegment(self, segname: str, padding: int=None) -> Tuple[bytes, int]:
        try:
            return self.segmentdict[segname].extract_segment(padding)
        except:
            print("Segment not found.")
            return None

    ''' returns section data or None if not found. '''
    def ExtractSection(self, segname: str, sectname: str) -> bytes:
        try:
            return self.segmentdict[segname].extract_section(sectname)
        except:
            print("Section not found.")
            return None

def GenerateFileFromDiagram(diagram: dict, macho: MachoFile, outputfile : io.BufferedIOBase,
                            padding: int=0) -> None:
    ''' Generate an output file from the diagram and input data. '''
    ''' Returns a list of lines of text which textually describes the entire output
        including a hex dump. '''
    relocationtableheaderpack = '<III'
    relocationtableentrypack = '<III'
    relocationtablezeropack = '<II'

    description = []

    numcomponents = 0
    relocationsegments = 0
    zeroinitcommands = 0
    for r in (diagram['regions']):
        componentlist = r['components']
        for comp in (componentlist):
            try:
                segname = comp['segment']
            except KeyError:
                pass
            try:
                reserverelocation = comp['emitrelocation']
            except KeyError:
                reserverelocation = (0 != numcomponents) and (segname != relocationsegmentname)
            numcomponents += 1
            if reserverelocation:
                relocationsegments += 1
                zeroinitcommands += 1

    relocationtablesize = struct.calcsize(relocationtableheaderpack) \
                          + relocationsegments * struct.calcsize(relocationtableentrypack) \
                          + zeroinitcommands * struct.calcsize(relocationtablezeropack)

    try:
        componentfilealignment = diagram['filealign']
    except KeyError:
        componentfilealignment = 8
    try:
        componentmemoryalignment = diagram['memalign']
    except KeyError:
        componentmemoryalignment = 8

    itemmap = []
    relocationtable = []
    zerotable = []

    # create a map
    firstsegmentbaseaddress = diagram['regions'][0]['baseaddress']
    try:
        fileloadaddress = diagram['loadaddress']
    except:
        fileloadaddress = firstsegmentbaseaddress
    fileoffset = firstsegmentbaseaddress - fileloadaddress
    for r in (diagram['regions']):
        componentlist = r['components']
        memaddress = r['baseaddress']
        for comp in (componentlist):
            segname = comp['segment']
            sectname = comp['section'] if 'section' in comp else None
            segleninfile = 0
            if (segname == relocationsegmentname):
                comp['emitrelocation'] = False
                comp['expandzeroes'] = False
                segleninfile = relocationtablesize
                segleninmemory = segleninfile
            else:
                segleninfile = 0
                segleninmemory = 0
                if sectname is not None:
                    try:
                        _, segleninfile = macho.segmentdict[segname].sectiondict[sectname].segment_extent()
                        segleninmemory = segleninfile
                    except:
                        pass
                else:
                    try:
                        _, segleninfile, numzeroes = macho.segmentdict[segname].stream_extent()
                        segleninmemory = segleninfile + numzeroes
                    except:
                        pass
            fileoffset = round_up_to_alignment(fileoffset, componentfilealignment)
            memaddress = round_up_to_alignment(memaddress, componentmemoryalignment)
            effectivefileaddress = fileoffset + fileloadaddress
            try:
                if comp['expandzeroes']:
                    segleninfile = segleninmemory
            except:
                pass
            segmentop = (segname, sectname, fileoffset, memaddress, segleninfile, segleninmemory)
            itemmap.append(segmentop)
            try:
                emitrelocation = comp['emitrelocation']
            except KeyError:
                emitrelocation = (memaddress != effectivefileaddress)
            if emitrelocation and (segname != relocationsegmentname):
                relocop = (effectivefileaddress, segleninfile, memaddress)
                relocationtable.append(relocop)
                if (segleninfile < segleninmemory):
                    effectivefilezeroaddress = memaddress + segleninfile
                    memoryzerolen = segleninmemory - segleninfile
                    zeroop = (memoryzerolen, effectivefilezeroaddress)
                    zerotable.append(zeroop)

            fileoffset += segleninfile
            memaddress += segleninmemory

    # build relocation data structure
    relocationstructure = struct.pack(relocationtableheaderpack,relocationmaptype,len(relocationtable),len(zerotable))
    relocationexplanation = [ "The relocation structure starts with a header indicating the type of the table (0x{:08x})\n".format(relocationmaptype),
                              "the number of relocations (0x{:08x})\n".format(len(relocationtable)),
                              "and the number of ranges to be zeroed out (0x{:08x})\n".format(len(zerotable)) ]

    for relo in (relocationtable):
        relocationstructure += struct.pack(relocationtableentrypack,*relo)
        relocationexplanation.extend([ "Then a relocation entry indicating that file bytes 0x{:08x}-0x{:08x}\n".format(relo[0]-fileloadaddress,relo[0]-fileloadaddress+relo[1]-1),
                                       "(memory addresses 0x{:08x}-0x{:08x})\n".format(relo[0],relo[0]+relo[1]-1),
                                       "should be relocated to memory addresses 0x{:08x}-0x{:08x}\n".format(relo[2],relo[2]+relo[1]-1) ])
    for zeroe in (zerotable):
        relocationstructure += struct.pack(relocationtablezeropack,*zeroe)
        relocationexplanation.extend([ "Then a clearing entry indicating that memory addresses 0x{:08x}-0x{:08x}\n".format(zeroe[1],zeroe[1]+zeroe[0]-1),
                                       "should be zeroed out\n" ])

    filewriteoffset = 0
    for segmentname, sectionname, segmentfileoffset, memaddress, segleninfile, _ in (itemmap):
        if filewriteoffset < segmentfileoffset:
            segmentpaddinglen = segmentfileoffset - filewriteoffset
            segmentpaddingbytes = bytes((padding,)) * segmentpaddinglen
            outputfile.write(segmentpaddingbytes)
            description.append('This is 0x{:x} bytes of 00 padding to advance to the necessary file offset for the next segment\n'.format(segmentpaddinglen))
            description.append(hexdump.hexdump(segmentpaddingbytes,startoffset=filewriteoffset))
            filewriteoffset = segmentfileoffset
        if segmentname == relocationsegmentname:
            outputfile.write(relocationstructure)
            description.append('This is the relocation structure used to describe how to expand this file to the correct in-memory representation\n')
            description.extend(relocationexplanation)
            description.append(hexdump.hexdump(relocationstructure,startoffset=filewriteoffset))
            filewriteoffset += len(relocationstructure)
        else:
            if sectionname is not None:
                comptowrite = macho.ExtractSection(segmentname,sectionname)
                description.append('This is the contents of the section named {}:{}\n'.format(segmentname,sectionname))
            else:
                comptowrite, _ = macho.ExtractSegment(segmentname)
                description.append('This is the contents of the segment named {}\n'.format(segmentname))
            outputfile.write(comptowrite)
            description.append(hexdump.hexdump(comptowrite,startoffset=filewriteoffset))
            if len(comptowrite) < segleninfile:
                segmentpaddinglen = segleninfile - len(comptowrite)
                segmentpaddingbytes = bytes((padding,)) * segmentpaddinglen
                outputfile.write(segmentpaddingbytes)
                description.append('followed by 0x{:x} bytes of segment padding\n'.format(segmentpaddinglen))
                description.append(hexdump.hexdump(segmentpaddingbytes,startoffset=filewriteoffset))
            filewriteoffset += segleninfile
        filewritepadlen = filewriteoffset % componentfilealignment
        if filewritepadlen != 0:
            filewritepadlen = componentfilealignment - filewritepadlen
            filewritepadbytes = bytes((padding,) * filewritepadlen)
            outputfile.write(filewritepadbytes)
            description.append('followed by 0x{:x} bytes of file alignment padding\n'.format(filewritepadlen))
            description.append(hexdump.hexdump(filewritepadbytes,startoffset=filewriteoffset))
            filewriteoffset += filewritepadlen

    return description

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Inspect and parse a Mach-o file and display and extract metadata and contents.'
                                                 'Can also assemble an output file with expanded segments and relocation informaton.',
                                     epilog='sections specified as <SEGNAME>:<SECTNAME>')
    parser.add_argument('inputfile', help='input mach-o to operate on')
    parser.add_argument('--listsegments', action='store_true')
    parser.add_argument('--listsections', action='store_true')
    parser.add_argument('--porcelain',action='store_true', help='tailor output text to scripts, not humans')
    parser.add_argument('--xsegment','--extractsegment', help='extract a segment')
    parser.add_argument('--xsection','--extractsection', help='extract a section')
    parser.add_argument('--hexoutput', action='store_true', help='when extracting single segment or section, extract to hexdump output instead of binary.')
    parser.add_argument('--padx', action='store_true', help='when extracting a section expand the unstored pad bytes')
    parser.add_argument('--outputfile','--output',help='output file for extractions (not metadata lists)')
    parser.add_argument('--diagram', help='supply a file which specifies which segments to extract and reassemble')
    parser.add_argument('--explain', help='(diagrams only) Print an explanation of every byte in the outpufile', action='store_true')
    args = parser.parse_args()

    padding = 0 if args.padx else None

    parsed = MachoFile(filename=args.inputfile)

    outputlocation = None
    sysoutlocation = sys.stdout
    sysoutbinlocation = sys.stdout.buffer

    if (args.outputfile):
        outputlocation = open(args.outputfile,"w" if args.hexoutput else "wb")
        if args.hexoutput:
            sysoutlocation = outputlocation
        else:
            sysoutbinlocation = outputlocation

    if args.xsegment is not None:
        extractedsegment, amtpadding = parsed.ExtractSegment(args.xsegment,padding)
        if (args.hexoutput):
            sysoutlocation.write(hexdump.hexdump(extractedsegment))
            if amtpadding > 0:
                sysoutlocation.write('and 0x{:0x} bytes of 00 pad\n'.format(amtpadding))
        else:
            sysoutbinlocation.write(extractedsegment)

    if args.xsection is not None:
        segname, sectname = args.xsection.split(':')
        extractedsection = parsed.ExtractSection(segname,sectname)
        if (args.hexoutput):
            sysoutlocation.write(hexdump.hexdump(extractedsection))
        else:
            sysoutbinlocation.write(extractedsection)

    if args.diagram is not None:
        with open(args.diagram,"r") as jsonfile:
            diagram = json.load(jsonfile)
            explanation = GenerateFileFromDiagram(diagram,parsed,sysoutbinlocation)
            if args.explain:
                print('\n'.join(explanation))
            pass

    if args.listsegments:
        if args.porcelain:
            parsed.PorcelainPrintMachoSegments()
        else:
            parsed.PrettyPrintMachoSegments()

    if args.listsections:
        if args.porcelain:
            parsed.PorcelainPrintMachoSections()
        else:
            parsed.PrettyPrintMachoSections()

    if outputlocation is not None:
        outputlocation.close()
