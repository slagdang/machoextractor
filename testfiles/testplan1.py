#!/usr/bin/env python3

"""
This work is available under a Creative Commons Attribution 4.0 International License (CC BY 4.0)

For the purposes of this license the title of this work is Macho Extractor.

It is licensed by Steve Bollinger and the original work was published (and may still be available)
at https://bitbucket.org/slagdang/machoextractor .

Information on the CC BY 4.0 license is available at http://creativecommons.org/licenses/by/4.0/
in both human-readable form and fully specified legal terms.
"""

import sys
import site
from hashlib import sha1

site.addsitedir("..")

from machocontainer import MachoFile

testfilemacho = MachoFile(filename="createtextmacho.o")

segmentsha = sha1()
segmenthashexpected = '3a699dc42d8a8db29458a772fdbbdd802fe75935'
segmentdata, zerocount = testfilemacho.ExtractSegment('')
segmentsha.update(segmentdata)
segmentsha.update(zerocount.to_bytes(8,'little'))
segmenthashproduced = segmentsha.hexdigest()

if (segmenthashproduced != segmenthashexpected):
    raise Exception(f'Segment hash was {segmenthashproduced} expected {segmenthashexpected}')

sectionvector = (('__TEXT','__const','ce713a1b680af32278a781d04f5b510309bb14a2'),
 ('__TEXT','__text','3a94b520f32c0a3e2b8014bc47d7c8ed78b096d9'),
 ('__DATA','__data','231732deee8be56540b25849a166e7a102ac21cb'),
 ('__DATA','__zerofill','da39a3ee5e6b4b0d3255bfef95601890afd80709'),
 ('__LPDATA','_lpram','e9d533bd5bf846f34ec37e623d38ad812e3fa038'))

for _, sectname, expectedhash in (sectionvector):
    sectionsha = sha1()
    segmentdata = testfilemacho.ExtractSection('',sectname)
    sectionsha.update(segmentdata)
    producedhash = sectionsha.hexdigest()
    if (producedhash != expectedhash):
        raise Exception(f'Section (\'\',\'{sectname}\') hash was {producedhash} expected {expectedhash}')

print('Test succeeded')
