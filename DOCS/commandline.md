# How to use machocontainer.py

All command examples work when executed from the same directory as this explanation document.

## Help

    ../machocontainer.py --help

## Examining data

You can list the segments in a macho.

    ../machocontainer.py --listsegments createtextmacho

output:

    segment name            mem address           mem size        file offset           file len
    __DATA           0x00000001000004e0 0x0000000000000220 0x00000000000004e0 0x0000000000000030
    __LINKEDIT       0x0000000100000730 0x00000000000000d0 0x0000000000000630 0x00000000000000c8
    __LPDATA         0x0000000100000700 0x0000000000000030 0x0000000000000600 0x0000000000000030
    __PAGEZERO       0x0000000000000000 0x0000000100000000 0x0000000000000000 0x0000000000000000
    __TEXT           0x0000000100000000 0x00000000000004e0 0x0000000000000000 0x00000000000004e0

You can list the sections in a macho.

    ../machocontainer.py --listsections createtextmacho

output:

    segment name     section name            mem address           mem size        file offset           file len
    __DATA           <<SEGMENT>>      0x00000001000004e0 0x0000000000000220 0x00000000000004e0 0x0000000000000030
    __DATA           __common         0x0000000100000600 0x00000000000000fa 0x0000000000000000 0x0000000000000000
    __DATA           __data           0x00000001000004e0 0x000000000000002c 0x00000000000004e0 0x000000000000002c
    __DATA           __zerofill       0x000000010000050c 0x0000000000000000 0x000000000000050c 0x0000000000000000
    __LINKEDIT       <<SEGMENT>>      0x0000000100000730 0x00000000000000d0 0x0000000000000630 0x00000000000000c8
    __LPDATA         <<SEGMENT>>      0x0000000100000700 0x0000000000000030 0x0000000000000600 0x0000000000000030
    __LPDATA         _lpram           0x0000000100000700 0x0000000000000028 0x0000000000000600 0x0000000000000028
    __PAGEZERO       <<SEGMENT>>      0x0000000000000000 0x0000000100000000 0x0000000000000000 0x0000000000000000
    __TEXT           <<SEGMENT>>      0x0000000100000000 0x00000000000004e0 0x0000000000000000 0x00000000000004e0
    __TEXT           __const          0x00000001000004a4 0x000000000000003c 0x00000000000004a4 0x000000000000003c
    __TEXT           __text           0x0000000100000484 0x0000000000000020 0x0000000000000484 0x0000000000000020

## Extracting data (single items)

You can extract the data in a section or segment to a binary output file.

    ../machocontainer.py --xsection __TEXT:__const ../testfiles/createtextmacho --outputfile /tmp/constsection.bin

You can extract (display) the data as a hex dump.

    ../machocontainer.py --xsection __TEXT:__const ../testfiles/createtextmacho --hexoutput

output:

    00000000  54 68 69 73 20 69 73 20 5f 5f 54 45 58 54 3a 5f
    00000010  5f 63 6f 6e 73 74 00 00 01 00 00 00 02 00 00 00
    00000020  03 00 00 00 04 00 00 00 05 00 00 00 06 00 00 00
    00000030  07 00 00 00 08 00 00 00 09 00 00 00

It is possible that a segment to be extracted is not fully stored in the macho. The remaining data is assumed to be all zeroes. Unless the --padx option is passed the zeroes will not be extracted.

## Extracting data to construct a binary image.

If you are using this to create a binary image for loading to memory (RAM or storing in flash) you can create a diagram file to assemble the sections you want into a particular arrangement.

This layout is described in a diagram file. The format of the diagram file is described in diagramfile.md.

You can create a binary image using the diagram and the source macho.

    ../machocontainer.py --diagram ../testfiles/diagram.json ../testfiles/createtextmacho --outputfile /tmp/diagram.bin

For an explanation of the contents of the output binary image you can also pass --explain

    ../machocontainer.py --diagram ../testfiles/diagram.json ../testfiles/createtextmacho --outputfile /tmp/diagram.bin --explain

output:

    This is the contents of the segment named __TEXT

    00000000  cf fa ed fe 07 00 00 01 03 00 00 00 02 00 00 00
    00000010  09 00 00 00 40 04 00 00 01 00 00 00 00 00 00 00
    00000020  19 00 00 00 48 00 00 00 5f 5f 50 41 47 45 5a 45
    00000030  52 4f 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000040  00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00
    00000050  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000060  00 00 00 00 00 00 00 00 19 00 00 00 e8 00 00 00
    00000070  5f 5f 54 45 58 54 00 00 00 00 00 00 00 00 00 00
    00000080  00 00 00 00 01 00 00 00 e0 04 00 00 00 00 00 00
    00000090  00 00 00 00 00 00 00 00 e0 04 00 00 00 00 00 00
    000000a0  05 00 00 00 05 00 00 00 02 00 00 00 00 00 00 00
    000000b0  5f 5f 74 65 78 74 00 00 00 00 00 00 00 00 00 00
    000000c0  5f 5f 54 45 58 54 00 00 00 00 00 00 00 00 00 00
    000000d0  84 04 00 00 01 00 00 00 20 00 00 00 00 00 00 00
    000000e0  84 04 00 00 02 00 00 00 00 00 00 00 00 00 00 00
    000000f0  00 04 00 80 00 00 00 00 00 00 00 00 00 00 00 00
    00000100  5f 5f 63 6f 6e 73 74 00 00 00 00 00 00 00 00 00
    00000110  5f 5f 54 45 58 54 00 00 00 00 00 00 00 00 00 00
    00000120  a4 04 00 00 01 00 00 00 3c 00 00 00 00 00 00 00
    00000130  a4 04 00 00 02 00 00 00 00 00 00 00 00 00 00 00
    00000140  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000150  19 00 00 00 38 01 00 00 5f 5f 44 41 54 41 00 00
    00000160  00 00 00 00 00 00 00 00 e0 04 00 00 01 00 00 00
    00000170  20 02 00 00 00 00 00 00 e0 04 00 00 00 00 00 00
    00000180  30 00 00 00 00 00 00 00 03 00 00 00 03 00 00 00
    00000190  03 00 00 00 00 00 00 00 5f 5f 64 61 74 61 00 00
    000001a0  00 00 00 00 00 00 00 00 5f 5f 44 41 54 41 00 00
    000001b0  00 00 00 00 00 00 00 00 e0 04 00 00 01 00 00 00
    000001c0  2c 00 00 00 00 00 00 00 e0 04 00 00 02 00 00 00
    000001d0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    000001e0  00 00 00 00 00 00 00 00 5f 5f 7a 65 72 6f 66 69
    000001f0  6c 6c 00 00 00 00 00 00 5f 5f 44 41 54 41 00 00
    00000200  00 00 00 00 00 00 00 00 0c 05 00 00 01 00 00 00
    00000210  00 00 00 00 00 00 00 00 0c 05 00 00 02 00 00 00
    00000220  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000230  00 00 00 00 00 00 00 00 5f 5f 63 6f 6d 6d 6f 6e
    00000240  00 00 00 00 00 00 00 00 5f 5f 44 41 54 41 00 00
    00000250  00 00 00 00 00 00 00 00 00 06 00 00 01 00 00 00
    00000260  fa 00 00 00 00 00 00 00 00 00 00 00 08 00 00 00
    00000270  00 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00
    00000280  00 00 00 00 00 00 00 00 19 00 00 00 98 00 00 00
    00000290  5f 5f 4c 50 44 41 54 41 00 00 00 00 00 00 00 00
    000002a0  00 07 00 00 01 00 00 00 30 00 00 00 00 00 00 00
    000002b0  00 06 00 00 00 00 00 00 30 00 00 00 00 00 00 00
    000002c0  03 00 00 00 03 00 00 00 01 00 00 00 00 00 00 00
    000002d0  5f 6c 70 72 61 6d 00 00 00 00 00 00 00 00 00 00
    000002e0  5f 5f 4c 50 44 41 54 41 00 00 00 00 00 00 00 00
    000002f0  00 07 00 00 01 00 00 00 28 00 00 00 00 00 00 00
    00000300  00 06 00 00 02 00 00 00 00 00 00 00 00 00 00 00
    00000310  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000320  19 00 00 00 48 00 00 00 5f 5f 4c 49 4e 4b 45 44
    00000330  49 54 00 00 00 00 00 00 30 07 00 00 01 00 00 00
    00000340  d0 00 00 00 00 00 00 00 30 06 00 00 00 00 00 00
    00000350  c8 00 00 00 00 00 00 00 01 00 00 00 01 00 00 00
    00000360  00 00 00 00 00 00 00 00 02 00 00 00 18 00 00 00
    00000370  30 06 00 00 07 00 00 00 a0 06 00 00 58 00 00 00
    00000380  1b 00 00 00 18 00 00 00 9d 8b 6a dc 26 e0 38 27
    00000390  b0 42 c2 ed 52 52 5d b9 2a 00 00 00 10 00 00 00
    000003a0  00 00 00 00 00 00 00 00 05 00 00 00 b8 00 00 00
    000003b0  04 00 00 00 2a 00 00 00 00 00 00 00 00 00 00 00
    000003c0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    *
    00000430  00 00 00 00 00 00 00 00 84 04 00 00 01 00 00 00
    00000440  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    *
    00000480  00 00 00 00 54 68 69 73 20 69 73 20 5f 5f 54 45
    00000490  58 54 3a 5f 5f 74 65 78 74 00 66 90 aa 55 aa 55
    000004a0  55 aa 55 aa 54 68 69 73 20 69 73 20 5f 5f 54 45
    000004b0  58 54 3a 5f 5f 63 6f 6e 73 74 00 00 01 00 00 00
    000004c0  02 00 00 00 03 00 00 00 04 00 00 00 05 00 00 00
    000004d0  06 00 00 00 07 00 00 00 08 00 00 00 09 00 00 00

    This is the relocation structure used to describe how to expand this file to the correct in-memory representation

    The relocation structure starts with a header indicating the type of the table (0x00000001)

    the number of relocations (0x00000003)

    and the number of ranges to be zeroed out (0x00000001)

    Then a relocation entry indicating that file bytes 0x00000528-0x00000557

    should be relocated to addresses 0x20000000-0x2000002f

    Then a relocation entry indicating that file bytes 0x00000558-0x00000587

    should be relocated to addresses 0x30000000-0x3000002f

    Then a relocation entry indicating that file bytes 0x00000588-0x000005c3

    should be relocated to addresses 0x40000000-0x4000003b

    Then a clearing entry indicating that addresses 0x20000030-0x2000021f

    should be zeroed out

    000004e0  01 00 00 00 03 00 00 00 01 00 00 00 28 05 00 00
    000004f0  30 00 00 00 00 00 00 20 58 05 00 00 30 00 00 00
    00000500  00 00 00 30 88 05 00 00 3c 00 00 00 00 00 00 40
    00000510  f0 01 00 00 30 00 00 20

    This is 0x10 bytes of 00 padding to advance to the necessary file offset for the next segment

    00000518  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00

    This is the contents of the segment named __DATA

    00000528  54 68 69 73 20 69 73 20 5f 5f 44 41 54 41 3a 5f
    00000538  5f 64 61 74 61 00 00 00 09 00 00 00 07 00 00 00
    00000548  05 00 00 00 03 00 00 00 01 00 00 00 00 00 00 00

    This is the contents of the segment named __LPDATA

    00000558  54 68 69 73 20 69 73 20 5f 5f 4c 50 44 41 54 41
    00000568  3a 5f 6c 70 72 61 6d 00 02 00 00 00 04 00 00 00
    00000578  06 00 00 00 08 00 00 00 00 00 00 00 00 00 00 00

    This is the contents of the section named __TEXT:__const

    00000588  54 68 69 73 20 69 73 20 5f 5f 54 45 58 54 3a 5f
    00000598  5f 63 6f 6e 73 74 00 00 01 00 00 00 02 00 00 00
    000005a8  03 00 00 00 04 00 00 00 05 00 00 00 06 00 00 00
    000005b8  07 00 00 00 08 00 00 00 09 00 00 00

    followed by 0x4 bytes of file alignment padding

    000005c4  00 00 00 00
