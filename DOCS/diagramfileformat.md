# Diagram Files

If you are using this to create a binary image for loading to memory (RAM or storing in flash) you can create a diagram file to assemble the sections you want into a particular arrangement.

A diagram file is a JSON file which describes pertinent offsets, segments and sections. JSON files do not allow hexadecimal values or comments so all offsets must be converted to decimal and cannot even have a helpful hexadecimal equivalent comment next to them.

The file is processed in order and items will often have a default address equal to the address at which the previous item ended (in the file) rounded up to the file alignment. It will also have a memory address which is equal to the address at which the previous item ended (in memory) rounded up to the memory alignment.

## Diagram File Format

The top level of the diagram file is a named tuple. It can specify the following integer valued items.

"loadaddress": **integer** - Address in memory where the binary file will be loaded. If this is not specified the load address is assumed to be the start address of the first section/segment. It is not usual to specify this.  
"memalign": **integer** - Data (segments/sections) added to the file will be aligned to a final memory address which is a multiple of this number of bytes indicated.  
"filealign": **integer** - Data (segments/sections) added to the file will be aligned to a file offset which is a multiple of this number of bytes.

If there is to be any output in the output file the the top level must also contain a "regions" item which lists an array of named tuples which indicate what data to add to the file and the address at which to place it.

"regions":  
[  
  
]

Each region can have an item which specifies the base address of the region. Without this the region is assumed to be located after the previous region as described above.  
"baseaddress": **integer** - Address in memory where the region should be located.  
If there is to be any output in this region then the region must also have a "components" item which lists an array of named tuples which specify the segments and sections to copy to the region from the input macho. If the component is located at a different memory offset (from start) than file offset (from start) then a relocation will be emitted in the relocation region. (see below).

"components":  
[  
  
]

"segment": **string** - Name of a segment to add to the output file. The section name may be the special name $$RELOCATION$$ which indicates to emit the relocation table here. For Relocation info see below.  
"section": **string** - Name of a section (segment:section) to add to the output file.  
"expandzeroes": true - Indicate that the unrecorded zero-area of the segment specified should be included in the output file. Without this the zeroes are instead added to the list of ranges in the relocation information which are to be zeroed.  
"emitrelocation": true - Indicate that a relocation should be emitted for this component even if it would otherwise not be emitted. Generally this is a bad idea.

No component may include both a segment and a section or more than one segment or section. To have multiple segments or sections in a region simply list multiple components with no base addressess specified for any other than the first (and the first is also optional.

## Relocations and Relocation Tables

As each segment or section is added to the output file its offset relative to the start of the file is compared to its offset relative to the base address at which the file is to be loaded. If these offsets are different then the memory address at which the segment/section will be loaded when the file is read into memory to is different than the address it is expected to be at during execution. This is very common when the resulting file is to be written to flash (XIP flash) in a microcontroller. The code will be at the right place but the data section will be following the code when instead it must be in SRAM during execution.

To correct for this the mis-located sections must be copied from the their address when initially loaded to their final execution-time address. This is done during early boot, before any global variables are used. machocontainer.py will create a relocation table while assembling the binary output file. It will place this relocation table where specified with special segment "$$RELOCATION$$" indicated in the diagram. This table usually should be placed after all the segments/sections which can be loaded and used from an unrelocated (correct) address and before any of the segments/sections which will require relocation to another range.

To make this as plain as possible: if you are using a microcontroller with XIP (execute-in-place) flash the diagram should place the \_\_TEXT segment (and any other read-only/flash segments) first in the output and then specify the $$RELOCATION$$ after all those and before the \_\_DATA segment and any other read-write/RAM segments. Using the diagram.json example file with all but the first two regions deleted and the \_\_DATA region base address set to the base address of SRAM in the microcontroller is usually a great place to start. The third region in diagram.json is there for the special LPRAM region of memory used for DMA on ATMEL SAM D and L microcontrollers. The fourth region is simply an example of how to include a section instead of a segment (generally not recommended).

### Relocation Table Format

The relocation table format is defined by the included relocationmap.h file. The relocation map will be aligned to the memalign value and will begin with the RelocationMap32Header structure shown. This will be followed directly (no padding) by consecutive RelocationMap32Entry structures and ZeroMap32Entry structures. The number of each is indicated in the header structure.

### Relocation Tips

To find the relocation table find the end of the first region. If the first region contains only the TEXT segment then you can do this by importing a special linker symbol segment$end$\_\_TEXT into a global variable using a .S file. After obtaining the value from the global variable round this value up by the same amount as was specified as memalign when running machocontainer.py. Then walk the indicated structures and execute the indicated relocations and zero the indicated ranges. It may be necessary to run these in orders other than the order the entries are found in the relocation map. It may also be the case that your initial execution stack is located inside one of the ranges of relocations or zeroings you are expected to execute. Writing over your current execution stack will likely lead to a very difficult to explain crash after the relocations are completed.
