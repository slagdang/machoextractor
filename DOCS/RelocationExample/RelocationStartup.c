/*
 * This work is available under a Creative Commons Attribution 4.0 International License (CC BY 4.0)
 *
 * For the purposes of this license the title of this work is Macho Extractor.
 *
 * It is licensed by Steve Bollinger and the original work was published (and may still be available)
 * at https://bitbucket.org/slagdang/machoextractor .
 *
 * Information on the CC BY 4.0 license is available at http://creativecommons.org/licenses/by/4.0/
 * in both human-readable form and fully specified legal terms.
 */

#include <stdint.h>
#include <string.h>

#include "relocationmap.h"

typedef void (*constructor_func)(void);

// start and end (limit) of TEXT segment
// These are variables at those locations, so take pointers to these to compare
extern void *__seg_start_TEXT_seg;
extern void *__seg_end_TEXT_seg;

// This is the length of the TEXT segment. It is the actual value, don't take a pointer
// to this
#define __seg_len_TEXT_seg ((uintptr_t) &__seg_end_TEXT_seg - (uintptr_t) &__seg_start_TEXT_seg)

// start and end (limit) of DATA segment
// These are variables at those locations, so take pointers to these to compare
extern void *__seg_start_DATA_seg;
extern void *__seg_end_DATA_seg;

// This is the length of the DATA segment. It is the actual value, don't take a pointer
// to this.
#define __seg_len_DATA_seg ((uintptr_t) &__seg_end_DATA_seg - (uintptr_t) &__seg_start_DATA_seg)

// start and end (limit) of zerofill section in DATA segment
// These are variables at those locations, so take pointers to these to compare
extern void *__sec_start_DATA_zerofill;
extern void *__sec_end_DATA_zerofill;

// This is the length of the zerofill section of the DATA segment. It is the actual value
// don't take a pointer to this.
#define __sec_len_DATA_zerofill_sec ((uintptr_t) &__sec_end_DATA_zerofill - (uintptr_t) &__sec_start_DATA_zerofill)

// start and end (limit) of constructor section in DATA segment
extern void *__sec_start_DATA_constructor;
extern void *__sec_end_DATA_constructor;

// start and end (limit) of module init section in DATA segment
// These are variables at those locations, so take pointers to these to compare
extern void *__sec_start_DATA_mod_init_func;
extern void *__sec_end_DATA_mod_init_func;

extern void main(void);

void runtime_init(void)
{
    void            *findrelocation = __seg_end_TEXT_seg;

    {
        uintptr_t           rounduprelocation = (uintptr_t) findrelocation;
        uintptr_t           overamt = rounduprelocation % 8;

        if (overamt) // rounding value in diagram file
        {
            const uintptr_t         underamt = 8 - overamt;

            rounduprelocation += underamt;
            findrelocation = (void *) rounduprelocation;
        }
    }

    const RelocationMap32HeaderPtr  relocinfo = (const RelocationMap32HeaderPtr) findrelocation;
    findrelocation = (void *) ((uintptr_t) findrelocation + sizeof(RelocationMap32Header));
    const unsigned int              reloccount = relocinfo->numRelocationEntries;

    if (reloccount > 0)
    {
        RelocationMap32EntryPtr relocentry = findrelocation;

        for (unsigned int lp = 0; lp < reloccount; ++lp)
        {
            void            *destination = (void *) relocentry->destinationAddress;
            void            *destinationend = (void *) (relocentry->destinationAddress + relocentry->length);

            if ((void *) gStackUserLevel >= destination && (void *) gStackUserLevel < destinationend)
            {
                // This section includes our current stack. We have to avoid moving
                // data over the current stack or we won't be able to get back to
                // the calling function.
                uint32_t        amountbelowstack = (uint32_t) ((uintptr_t) gStackUserLevel - (uintptr_t) destination);
                uint32_t        offsetabovestack = (uint32_t) ((uintptr_t) gStackUserLevelBase - (uintptr_t) destination);

                memcpy(destination,(void*) (relocentry->loadedAddress),amountbelowstack);

                if (relocentry->length > offsetabovestack)
                {
                    uint32_t        amountabovestack = relocentry->length - offsetabovestack; 
                    void            *sourceabovestack = (void *) ((uintptr_t) relocentry->loadedAddress + offsetabovestack);

                    memcpy(gStackUserLevelBase,sourceabovestack,amountabovestack);
                }
            }
            else
            {
                memcpy((void *) (destination),(void *) (relocentry->loadedAddress),relocentry->length);
            }
            ++relocentry;
        }

        findrelocation = (void *) relocentry;
    }

    const unsigned int          zerocount = relocinfo->numZeroEntries;

    if (zerocount > 0)
    {
        ZeroMap32Ptr                zeroentry = findrelocation;

        for (unsigned int lp = 0; lp < zerocount; ++lp)
        {
            void            *destination = (void *) zeroentry->destinationAddress;
            void            *destinationend = (void *) (zeroentry->destinationAddress + zeroentry->length);

            if ((void *) gStackUserLevel >= destination && (void *) gStackUserLevel < destinationend)
            {
                // This section includes our current stack. We have to avoid writing
                // data over the current stack or we won't be able to get back to
                // the calling function.
                uint32_t        amountbelowstack = (uint32_t) ((uintptr_t) gStackUserLevel - (uintptr_t) destination);
                uint32_t        offsetabovestack = (uint32_t) ((uintptr_t) gStackUserLevelBase - (uintptr_t) destination);

                memset(destination,0,amountbelowstack);

                if (zeroentry->length > offsetabovestack)
                {
                    uint32_t        amountabovestack = zeroentry->length - offsetabovestack;

                    memset(gStackUserLevelBase,0,amountabovestack);
                }
            }
            else
            {
                memset((void *) (zeroentry->destinationAddress),0,zeroentry->length);
            }
            ++zeroentry;
        }
    }

    constructor_func        *ctor_start = __sec_start_DATA_constructor;
    constructor_func        *ctor_end = __sec_end_DATA_constructor;

    for (constructor_func *ctors = ctor_start; ctors < ctor_end; ctors++)
    {
        // call constructors
        (*ctors)();
    }

    constructor_func        *mod_start = __sec_start_DATA_mod_init_func;
    constructor_func        *mod_end = __sec_end_DATA_mod_init_func;

    for (constructor_func *mods = mod_start; mods < mod_end; mods++)
    {
        // call module init
        (*mods)();
    }

    (void) main();
}
