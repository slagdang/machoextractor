#pragma once
#ifndef _H_RELOCATIONMAP
#define _H_RELOCATIONMAP 1

#include <stdint.h>

/**********
 * LICENSE
 **********
 *
 * This information is specific for this file, relocationmap.h
 *
 * This file can be used or modified freely by anyone for any purpose with
 * no attribution or restriction.
 * It is placed into the public domain.
 */

/**********
 * This header describes the relocation map entries and will be emitted upon
 * request when making a memory layout from an input macho and a diagram file.
 * The relocation quasi-segment will have the RelocationMap32Header first
 * followed by the indicated number of relocation entries directly behind it
 * and then followed by the indicated number of zero entries directly behind
 * that.
 *
 * The indicated relocations should be executed with individual overlap-safe
 * operations (memmove) and in the order listed. After that the indicated
 * zero operations should be executed.
 * 
 * Do note that the initial stack of execution may be within one of these
 * regions to be operated on and this may present difficulties for the
 * code executing the moves and zeroings.
 **********/

typedef struct RelocationMap32Entry
{
    uint32_t                loadedAddress;
    uint32_t                length;
    uint32_t                destinationAddress;
} RelocationMap32Entry, *RelocationMap32EntryPtr;

typedef struct ZeroMap32Entry
{
    uint32_t                length;
    uint32_t                destinationAddress;
} ZeroMap32Entry, *ZeroMap32Ptr;

typedef struct RelocationMap32Header
{
    uint32_t                mapType; // see map types below
    uint32_t                numRelocationEntries;
    uint32_t                numZeroEntries;
} RelocationMap32Header, *RelocationMap32HeaderPtr;

// The map type will be stored in the native endian of the architecture.
// The top byte of the map type will never be anything but 00, the bottom
// byte will never be 00. If the map type is seen to not fit this format
// then the map was presumably written with the opposite endianness than
// the processor viewing it.
enum
{
// 32-bit version 001 is the version of the headers described in this
// version of this file.
    kMapType32bitversion001 = 0x00000001
};

#endif
