# MachoExtractor

## Description

This project contains tools which can be used to manipulate Mach-o object files. It can list (some) contents and extract and display data and metadata in the files.

It also can be used to create binary load-and-go files suitable for flashing into boot flash on microcontrollers.

This is part of a larger project to create a workable set of tools which can be used to develop for ARM microcontrollers using the Apple-provided compilers.

For more information on how to use these tools is contained in commandline.md in the DOCS folder.

## LICENSE

Some portions of this work has been placed into the public domain. This is noted in license sections of these files if present, most notably relocationmap.h. Those files are thus available under license but also are yours (but not exclusively yours) and thus are free to use for any purpose in any way.

This work is available under a Creative Commons Attribution 4.0 International License (CC BY 4.0)

For the purposes of this license the title of this work is Macho Extractor.

It is licensed by Steve Bollinger and the original work was published (and may still be available) at https://bitbucket.org/slagdang/machoextractor .

Information on the CC BY 4.0 license is available at http://creativecommons.org/licenses/by/4.0/ in both human-readable form and fully specified legal terms.

## LICENSE EXCEPTIONS

The file relocationmap.h was placed into the public domain as part of this project. It is thus yours (but not exclusively yours), it is everyone's. It is also available as part of this total work with the CC BY 4.0 license above and may be used in that fashion if that is preferable.
