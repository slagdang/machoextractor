#!/usr/bin/env python3

"""
This work is available under a Creative Commons Attribution 4.0 International License (CC BY 4.0)

For the purposes of this license the title of this work is Macho Extractor.

It is licensed by Steve Bollinger and the original work was published (and may still be available)
at https://bitbucket.org/slagdang/machoextractor .

Information on the CC BY 4.0 license is available at http://creativecommons.org/licenses/by/4.0/
in both human-readable form and fully specified legal terms.
"""

# An array of which hex values are safe to print
safedumpingbytes = frozenset(range(b' '[0],127))
replacementdumpchar = b'.'[0]
# A dictionary translating hex values to a value safe to print
dumptranslation = tuple([(i if i in safedumpingbytes else replacementdumpchar) for i in range(256)])

def makesafechardump(data: tuple) -> str:
    ''' Create a safe string dump of the input bytes data. This will omit dumps for 
    some bad characters. '''
    if data is None:
        return None
    translatedbytes = bytes([dumptranslation[i] for i in data])
    translatedstr = translatedbytes.decode('latin1')
    return translatedstr

def hexdumplist(data: tuple, startoffset: int=0, offsetwidth: int=8, bytesperline: int=16,
                offsetxes: bool=False, eliderepeats: bool=True, alignoffsets: bool=True,
                asciicolumns: bool=False) -> list[str]:
    ''' Create a list of strings which are a textual representation of a hex dump of
    input bytes. '''
    returnlist = []
    offsetalignment = bytesperline if alignoffsets else 1
    endrange = len(data)
    consecutiveidenticallines = 0
    prevlinecontents = None
    suppressedline = ''
    offsetprefix = '0x' if offsetxes else ''
    # skipl is -1 times the number of data bytes to skip dumping on the first line
    # This is used to ensure the values on the first line are aligned in columns
    # with the ones on later lines when the start offset is not an even multiple
    # of the number of data bytes per line.
    offset = -(startoffset % offsetalignment)
    if offset < 0:
        # special first line
        linebytes = tuple(data[0:bytesperline+offset])
        offsetstr = offsetprefix + '{:0{width}x}  '.format(startoffset+offset,width=offsetwidth)
        databyteshexpaddingl = ' ' * (-3 * offset)
        databyteshexstr =  ' '.join(['{:02x}'.format(a) for a in linebytes])
        databyteshexpaddingr = ''
        databytescharsection = ''
        if asciicolumns:
            databytescharpaddingl = ' ' * (-1 * offset) + '  \''
            databytescharstr = makesafechardump(linebytes)
            databytescharpaddingr = '\''
            databytescharsection = databytescharpaddingl + databytescharstr + databytescharpaddingr
        outputline = offsetstr + databyteshexpaddingl + databyteshexstr + databyteshexpaddingr + databytescharsection
        returnlist.append(outputline)
        startoffset += offset + bytesperline
        offset = len(linebytes)
    while (offset < endrange):
        linebytes = tuple(data[offset:offset+bytesperline])
        offsetstr = offsetprefix + '{:0{width}x}  '.format(startoffset,width=offsetwidth)
        databyteshexstr = ' '.join(['{:02x}'.format(a) for a in linebytes])
        databytescharsection = ''
        databyteshexpaddingr = ''
        if asciicolumns:
            databyteshexpaddingr = ' ' * (3 * (bytesperline-len(linebytes)))
            databytescharpaddingl = '  \''
            databytescharstr = makesafechardump(linebytes)
            databytescharpaddingr = '\''
            databytescharsection = databytescharpaddingl + databytescharstr + databytescharpaddingr
        outputline = offsetstr + databyteshexstr + databyteshexpaddingr + databytescharsection
        if eliderepeats and (prevlinecontents == linebytes) and (consecutiveidenticallines > 0):
            suppressedline = outputline
            if 2 == consecutiveidenticallines:
                returnlist.append('*')
            consecutiveidenticallines += 1
        else:
            if len(suppressedline):
                returnlist.append(suppressedline)
            returnlist.append(outputline)
            consecutiveidenticallines = 1
            prevlinecontents = linebytes
            suppressedline = ''
        offset += len(linebytes)
        startoffset += len(linebytes)
    if consecutiveidenticallines > 1:
        returnlist.append(suppressedline)
    return returnlist

def hexdump(data: tuple, startoffset: int=0, offsetwidth: int=8, bytesperline: int=16,
            offsetxes: bool=False, eliderepeats: bool=True, alignoffsets: bool=True,
            asciicolumns: bool=False):
    ''' Create a single which is a textual representation of a hex dump of
    input bytes. It will include newlines at the end of all lines. Unless
    the output is empty in which case it will have no newlines. '''
    hexlist = hexdumplist(data,startoffset,offsetwidth,bytesperline,offsetxes,
                          eliderepeats,alignoffsets,asciicolumns)
    if len(hexlist) == 0:
        return ""

    return "\n".join(hexlist) + "\n"

if __name__ == "__main__":
    vector18 = range(18)
    print(hexdump(vector18,startoffset=0x10000))
    vector5by16 = tuple(range(16)) * 5
    print(hexdump(vector5by16,startoffset=0x2005))
    vectoruuudu = tuple([*range(0,16),*range(1,17),*(tuple(range(2,18))) * 2,*range(3,19)])
    print(hexdump(vectoruuudu,startoffset=0x1004,alignoffsets=False))
    zeroesvector256 = tuple((0,) * 256)
    print(hexdump(zeroesvector256))
    mostlyzeroesvector260 = tuple(range(18)) + tuple((0,) * (260 - 18 - 9)) + tuple(range(9))
    print(hexdump(mostlyzeroesvector260))
    vector20 = range(11,211,10)
    print(hexdump(vector20,offsetwidth=16,startoffset=0x200000000,offsetxes=True,asciicolumns=True))
    asciivector = b'Alfred \x19. Thomas with the long ball to the corner.\x90\xa0\xb0!'
    print(hexdump(asciivector,offsetwidth=12,startoffset=0x03,asciicolumns=True))
